# NetCDF

[NetCDF](https://www.unidata.ucar.edu/software/netcdf/) (Network Common Data
Form) is a set of software libraries and machine-independent data formats that
support the creation, access, and sharing of array-oriented scientific data.
This includes the NetCDF library as well as the NetCDF Operators (NCO), Climate
Data Operators (CDO), NCCMP, and NCVIEW packages.

## Using NetCDF at NERSC

NetCDF libraries on the Cori and Perlmutter are provided by HPE, for each of the `PrgEnv`s.
To check the available Cray installations and versions use the following command:

```console
module avail cray-netcdf    (# on Cori)
module spider cray-netcdf   (# on Perlmutter)
```

No compile or link options are required as long as you use the Cray compiler
wrappers `ftn`, `cc`, and `CC`. Below is an example:

```console
module load cray-netcdf
ftn my_netcdf_code.f90
cc  my_netcdf_code.c
CC  my_netcdf_code.cpp
```

### Known issues on Cori

- The Cray HDF5 and NetCDF libraries may break with errors mentioning problems
  with file locking. See the [known issues] section of the HDF5 docs page.
  
  [known issues]: ../hdf5/index.md#known-issues-on-cori

## Other NetCDF tools at NERSC

NERSC provides several tools which can be useful for analyzing and manipulating
NetCDF data.

### NetCDF Operators (NCO)

The [NetCDF Operators (NCO)](http://nco.sourceforge.net/) are a suite of file
operators that facilitate manipulation and analysis of self-describing data
stored in the NetCDF or HDF4/5 formats.

To access NCO on Cori, load the `nco` module file.  

NCO on Perlmutter is installed within the E4S software package. Below are the steps
to access NCO on Perlmutter:

```console
module load e4s
spack env active -V gcc
spack load nco
module load cudatoolkit/11.5
```

There are many NetCDF Operator tools, including:

- ncap2: netCDF Arithmetic Processor
- ncatted: netCDF ATTribute EDitor
- ncbo: netCDF Binary Operator (addition, multiplication...)
- ncclimo: netCDF CLIMatOlogy Generator
- nces: netCDF Ensemble Statistics
- ncecat: netCDF Ensemble conCATenator
- ncflint: netCDF FiLe INTerpolator
- ncks: netCDF Kitchen Sink
- ncpdq: netCDF Permute Dimensions Quickly, Pack Data Quietly
- ncra: netCDF Record Averager
- ncrcat: netCDF Record conCATenator
- ncremap: netCDF REMAPer
- ncrename: netCDF RENAMEer
- ncwa: netCDF Weighted Averager

Find usage information via `<nco_tool_name> --help`.

### NCCMP

The [NCCMP](https://gitlab.com/remikz/nccmp) tool compares two NetCDF files
bitwise or with a user-defined tolerance (absolute or relative percentage). To
use NCCMP, just load the nccmp module file with the `module load nccmp`
command. This command will automatically load the NetCDF module file.

To access NCO on Cori, load the `nccmp` module file.  

NCCMP on Perlmutter is installed within the E4S software package. Below are the steps
to access NCCMP on Perlmutter:

```console
module load e4s
spack env active -V gcc
spack load nccmp
```

NCCMP usage:

```console
nccmp [OPTION...] file1 [file2]
```

Find more information via `nccmp --help`.

### CDO

[Climate Data Operators
(CDO)](https://code.mpimet.mpg.de/projects/cdo/wiki/Cdo#Documentation) is a
large set of tools for working on climate data. NetCDF 3/4, GRIB (including
SZIP compression), EXTRA, SERVICE, and IEG are supported as I/O-formats. CDO
can also be used to analyse any kind of gridded data not related to climate
science. CDO has very small memory requirements and can process files larger
than physical memory.

To access NCO on Cori, load the `cdo` module file.  

CDO on Perlmutter is installed within the E4S software package. Below are the steps
to access CDO on Perlmutter:

```console
module load e4s
spack env active -V gcc
spack load cdo
```

CDO usage:

```console
cdo  [Options]  Operator1  [-Operator2  [-OperatorN]]
```

Find more information via `cdo --help`.

### NCView

[NCView](http://meteora.ucsd.edu/~pierce/ncview_home_page.html) is a visual
browser for NetCDF format files. To use NCView, first set up the necessary X11
environment (NERSC strongly recommends using
[NoMachine](../../../connect/nx.md)). Or use `ssh -Y cori.nersc.gov` or 
`ssh -Y perlmutter-p1.nersc.gov` to connect to NERSC.

To access NCView on Cori, load the `ncview` module file.  

NCView on Perlmutter is installed within the E4S software package. Below are the steps
to access NCView on Perlmutter:

```console
module load e4s
spack env active -V gcc
spack load ncview
```

NCView usage:

```console
ncview [options] datafiles
```

Find more information via `ncview --help`.

### NetCDF4-python

[netCDF4-python](https://unidata.github.io/netcdf4-python/netCDF4/index.html)
is an object-oriented python interface to the NetCDF library. It is available
via the `python` module.

!!! Note "netCDF4-python supports various classic netcdf versions"
    netCDF4-python also supports `netcdf3` and `netcdf3-classic`. Please make
    sure the format is consistent when you read and write the data.
