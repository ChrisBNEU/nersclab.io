# WRF

## Build WRF 

### Required modules

The majority of the WRF model code is written in Fortran, but some part and ancillary programs are written in C
 ([WRF UG ch.2](https://www2.mmm.ucar.edu/wrf/users/docs/user_guide_v4/v4.4/users_guide_chap2.html)). 
 For most cases, we run WRF using either shared-memory parallelism using the OpenMP application programming interface,  
 distributed memory message passing (MPI) parallelism across nodes, or both of them as hybrid. Therefore, we need to 
 use Fortran and C compilers that supports OpenMP along with the MPI library. For compiling such a complex program, 
 NERSC provides [compiler wrappers](../../development/compilers/wrappers.md) that combine compilers 
 and various libraries (including MPI) necessary to run shared- and distributed-memory program on the NERSC systems. 

In addition, WRF requires the netCDF library for input and output. WRF can use the 
[parallel netcdf library](https://parallel-netcdf.github.io/) to read/write netcdf files through 
multiple MPI tasks simultaneously, taking advantage of the
 [Lustre file system ](../../performance/io/lustre/index.md) of Cori and Perlmutter 
 [scratch space](../../filesystems/cori-scratch.md). WRF can also use the file compression functionality 
 from the netCDF4.0 or later version, which depends on the 
 [HDF5 library ](../../development/libraries/hdf5/index.md). 
 See [Balle & Johnsen (2016)](#reference) for WRF I/O options and their performance. 

!!! note
    NetCDF4 (and underlying HDF5) library provides parallel read/write functionality, which is 
    currently available as one of I/O options in WRF
    ([README.netcdf4par](https://github.com/wrf-model/WRF/blob/master/doc/README.netcdf4par)). 
    However, experiments by a WRF-SIG member found that the netcdf4 parallel I/O is significantly 
    slower than the I/O using the parallel netcdf library.  
   
!!! note
    Another useful knowledge about netCDF library is the limitation on the size of the variable in a file, which 
	depends on the netCDF data format (CDF1 = Classic ->2GB, CDF2=64-bit offset ->4GB, netCDF4 and CDF5 -> unlimited). 
	See the table "Large File Support" in the 
	[NetCDF Users Guide ](https://docs.unidata.ucar.edu/nug/current/file_structure_and_performance.html). 
	The current WRF code supports serial I/O of CDF1, CDF2, and netCDF4. 
	The WRF's interface to the parallel netcdf library supports CDF1 and CDF2. 

!!! note
    If a user runs a high-resolution, large-domain simulation with the number of columns greater than ~1500 x ~1500, 
	a 3D variable will be larger than 4GB and it is necessary to modify WRF's I/O source code to use the CDF5 format.

Our experience shows that using the netCDF and parallel netcdf libraries provide flexible 
(we can specify either serial netcdf or parallel netcdf in the run-time WRF namelist) and much faster 
(parallel netCDF I/O is 10--20 times faster than serial netCDF I/O) I/O on the scratch system. 
Therefore, we recommend to build WRF with the netCDF (cray-netcdf module) and parallel netCDF 
(cray-parallel-netcdf module) libraries, and specify the CDF2 format for high-resolution simulations. 
This I/O choice is activated by setting a few environmental variables when compiling WRF after loading 
the netcdf and parallel netcdf libraries: 

With these two modules, we set the following environmental variables when compiling WRF

```bash
...
module load cray-hdf5   #the netcdf library depends on hdf5
module load cray-netcdf
module load cray-parallel-netcdf
...

export NETCDF_classic=1               #use classic (CDF1) as default
#use 64-bit offset format (CDF2) of netcdf files
export WRFIO_NCD_LARGE_FILE_SUPPORT=1 
#netcdf4 compression (serial) with the hdf5 module can be very slow
export USE_NETCDF4_FEATURES=0
```

and then at run-time specify the namelist variables io_form_history, io_form_restart, etc., 
to be 11 for parallel netcdf I/O and 2 for standard serial netcdf. 

!!! note
    [A discussion in the WRF user forum]
    (https://forum.mmm.ucar.edu/threads/solved-netcdf-error-when-attempting-to-using-pnetcdf-quilting-with-wrf.9015/) 
    suggests that not only wrfinput but also wrfbdy data can be read through 
    the parallel netcdf library if WPS is compiled appropriately with the parallel netcdf.

#### Example module loading script

```bash
#!/bin/bash
set -e
#command example
#./load_modules_2022-12.sh pm 
#./load_modules_2022-12.sh knl 
#./load_modules_2022-12.sh hw 

machname=$1 #first input argument, "hw" (Haswell) "knl" (Knights Landing) or "pm" (Perlmutter)

scname=$BASH_SOURCE  #name of this script

echo "loading modules using ${scname}"
echo "machine name (hw (Haswell), knl (Knights Landing) or pm (Perlmutter)): $machname "


#general modules
if [ "$machname" = "pm" ]; then  #Perlmutter

    #1) unload unnecessary modules that each user added by .bash_profile
	# each user has to edit here
	
	#keep other automatically loaded modules 
	
	#2) load/switch to specific modules needed; 
	#specific versions not necessary but helps later reproducibility & debugging
	module load cpu
	 
    module load PrgEnv-gnu 
	module load gcc 
	module load cray-mpich
	module load craype
    #module load craype-hugepages64M #hugepages may not be needed for Perlmutter
    #also encountered run-time error with hugepages module after the 2022-12 maintenance
	
elif [ "$machname" = "hw" ] || [ "$machname" = "knl" ]; then #Cori

	#1) unload unnecessary modules that each user added by .bash_profile
	# each user has to edit here
	module unload nco
	module unload ncview
	module unload ncl

	#keep all the other automatically loaded modules 
	
	#2) load/switch to specific modules needed; 
	#specific versions not necessary but helps later reproducibility & debugging
	
	module switch PrgEnv-intel 
	module switch intel
	module switch cray-mpich
	module switch craype
	
	#for WPS 
	echo "loading WPS dependencies: zlib, png, jasper"
	module load zlib
	module load png
	module load jasper
	
	
	if [ "$machname" = "knl" ]; then 
		module switch craype-haswell craype-mic-knl  
	fi
	
	#test
	module switch craype-hugepages2M craype-hugepages128M #for heavy I/O
	
else
	echo "the machname argument is not a valid system name"
	exit 11
fi
	
	
#module for WRF file I/O
#default modules in August 2022
if [ "$machname" = "pm" ]; then  #Perlmutter
    #for netcdf4 large file & parallel IO
    #order of loading matters!
 
    module load cray-hdf5  #required to load netcdf library
    module load cray-netcdf	
    module load cray-parallel-netcdf
    


elif [ "$machname" = "hw" ] || [ "$machname" = "knl" ]; then #Cori
    module load cray-hdf5   #dependency of the netcdf library
    module load cray-netcdf         
    module load cray-parallel-netcdf
fi


module unload darshan  #can lead to a run-time error from mpich after 2022 March update
module list
```

### Perlmutter

WRF's build process starts with running the "configure" csh script that comes with the WRF source code package. 
This script automatically checks the computing platform and asks for a user input about the parallel job configuration.

On Perlmutter, we have tested the default gnu environment. 
Tested inputs to the "configure" csh script are gnu (dm+sm) and basic 
nesting:

```
Please select from among the following Linux x86_64 options:
....
32. (serial)  33. (smpar)  34. (dmpar)  35. (dm+sm)   GNU (gfortran/gcc)
...
Enter selection [1-75] :
Compile for nesting? (0=no nesting, 1=basic,...) [default 0] :
```

For real cases (not idealized cases like the 2d squall line), we recommend the option 35 (dm+sm) 
based on our experience of 4 threads per MPI rank (dm+sm) performing better than the pure MPI 
(dm) using the same number of nodes. The same applies to KNL.
We will update the WRF performance evaluation & scaling on Perlmutter in early 2023.

After running the configure program, we run the "compile" csh script in the top directory
 of the WRF source code. In the example bash script below, we do this by setting the following

```bash

doclean_all=false #true if compiled different configure options

doclean=false

runconf=false

docompile=false

```

The compile script does several checks and invokes the make command, among other things.

#### Example WRF build script for Perlmutter

```bash
#!/bin/bash -l
set -e
set -o pipefail 

imach="pm"

doclean_all=true #true if compiled different configure options

doclean=false

runconf=true

docompile=false

debug=false  #true to compile WRF with debug flag (no optimizations, -g flag for debugger, etc.)

# WRF directories
mversion="v4.4"
#WRF-SIG project directory as example; accessible only by WRF-SIG members
wrfroot="/global/cfs/cdirs/m4232/model"  
script_dir="/global/cfs/cdirs/m4232/scripts/build"

export WRF_DIR=${wrfroot}/${mversion}/WRF

#Modules --------------------------------------------------------------------
modversion="2022-12"  #use year of major update that module (default) are introduced (INC0182147)
loading_script="${script_dir}/load_modules_${modversion}_wrfsig.sh"
source ${loading_script} ${imach}

#set environmental variables used by WRF build system, borrowing environmental variables 
#set by modules

export NETCDF_classic=1               #use classic (CDF1) as default
export WRFIO_NCD_LARGE_FILE_SUPPORT=1 #use 64-bit offset format (CDF2) of netcdf files
export USE_NETCDF4_FEATURES=0         #do not use netcdf4 compression (serial), need hdf5 module
#configure says WRF won't use netcdf4 compression, but I still see a flag
#NETCDF4_IO_OPTS = -DUSE_NETCDF4_FEATURES..., confusing..


export HDF5=$HDF5_DIR
export HDF5_LIB="$HDF5_DIR/lib"
export HDF5_BIN="$HDF5_DIR/bin"

export NETCDF=$NETCDF_DIR
export NETCDF_BIN="$NETCDF_DIR/bin"
export NETCDF_LIB="$NETCDF_DIR/lib"

#need to append "/gnu/9.1"
export PNETCDF=$CRAY_PARALLEL_NETCDF_DIR/gnu/9.1 


export LD_LIBRARY_PATH="/usr/lib64":${LD_LIBRARY_PATH}
#export PATH=${NETCDF_BIN}:${PATH}
export PATH=${NETCDF_BIN}:${HDF5_BIN}:${PATH}
export LD_LIBRARY_PATH=${NETCDF_LIB}:${LD_LIBRARY_PATH}


#other special flags to test
export PNETCDF_QUILT="0"  #not stable and best configuration not figured out on Perlmutter


echo "LD_LIBRARY_PATH: "$LD_LIBRARY_PATH
echo "PATH: "$PATH
echo "MANPATH: "$MANPATH

echo "NETCDF is $NETCDF"
echo "NETCDF_LIB is $NETCDF_LIB"

echo "HDF5 is $HDF5"
echo "HDF5_LIB is $HDF5_LIB"

echo "PNETCDF: ${PNETCDF}"
echo "PNETCDF_QUILT: ${PNETCDF_QUILT}"


#
##capture starting time for log file name
idate=$(date "+%Y-%m-%d-%H_%M")
#
##run make in the top directory
cd $WRF_DIR

if [ "$doclean_all" = true ]; then
    ./clean -a
    #"The './clean –a' command is required if you have edited the configure.wrf 
    #or any of the Registry files.", but this deletes configure.wrf....
    
fi

if [ "$doclean" = true ]; then
    ./clean
fi

#echo "running configure"
if [ "$runconf" = true ]; then

    if [ "$debug" = true ]; then
        echo "configure debug mode"
        ./configure -d
    else
        ./configure
    fi

   ##configure options selected are:
   # 32. (serial)  33. (smpar)  34. (dmpar)  35. (dm+sm)   GNU (gfortran/gcc)
   # choose 35 for real (not idealized) cases

    configfile="${WRF_DIR}/configure.wrf"
    
    #and edit the following in configure.wrf
    #from the original
    #SFC             =       gfortran
    #SCC             =       gcc
    #CCOMP           =       gcc
    #DM_FC           =       mpif90
    #DM_CC           =       mpicc

    #to the following (FC and CC with MPI)
    #SFC             =       gfortran
    #SCC             =       gcc
    #CCOMP           =       cc
    #DM_FC           =       ftn
    #DM_CC           =       cc
    
    if [ -f "$configfile" ]; then
        echo "editing configure.wrf"
        #need to remove -cc=$(SCC) in DM_CC
        sed -i 's/-cc=\$(SCC)/ /' ${configfile}
        sed -i 's/mpif90/ftn/' ${configfile}
        sed -i 's/mpicc/cc/' ${configfile}
                
        #also remove the flag -DWRF_USE_CLM from ARCH_LOCAL if not plan to use CLM4 land model
        #to speed up compilation
        sed -i 's/-DWRF_USE_CLM/ /' ${configfile} 
        
    fi
    
fi

if [ "$docompile" = true ]; then
    export J="-j 4"  #build in parallel
    echo "J = $J"
    
    bldlog=${script_dir}/compile_em_${idate}_${imach}.log
    echo  "compile log file is ${bldlog}"

    ./compile em_real &> ${bldlog}
    #./compile em_les &> ${bldlog}  #idealized LES case used for one of benchmark tests
       
    set +e 
    #grep command exits the script in case of nomatch after the 2022-12 maintenance
    grep "Problems building executables" compile_em_real_${idate}_${imach}.log
    RESULT=$?
    
    set -e  
    if [ $RESULT -eq 0 ]; then
        echo "compile failed, check ${bldlog}"      
    else
        echo "compile success"
        #sometimes renaming executable with descriptive informaton is useful
        #cp $WRF_DIR/main/ideal.exe $WRF_DIR/main/ideal_${idate}_${imach}.exe
        #cp $WRF_DIR/main/real.exe $WRF_DIR/main/real_${idate}_${imach}.exe
        #cp $WRF_DIR/main/wrf.exe $WRF_DIR/main/wrf_${idate}_${imach}.exe
        #cp $WRF_DIR/main/ndown.exe $WRF_DIR/main/ndown_${idate}_${imach}.exe
    fi

fi
```

As seen in the example script, the compiler names need to be edited in the configure.wrf file.
Specifically, we need to change the names of compilers used for MPI applications to those of compiler wrappers.

- change "mpif90" to "ftn" for the DM_FC flag
- change "mpicc" to "cc"  for the DM_FC flag
- keep SFC and SCC to be the base compiler (gfortran and gcc)

These edits allow us to compile WRF on the login node.

### Cori Haswell (HW) & Knight's Landing (KNL)

On Cori HW and KNL, we tested the default Intel environment:

```
Please select from among the following Linux x86_64 options:
....
64. (serial)  65. (smpar) 66. (dmpar)  67. (dm+sm)   INTEL (ifort/icc): HSW/BDW 
68. (serial)  69. (smpar) 70. (dmpar)  71. (dm+sm)   INTEL (ifort/icc): KNL MIC
...
Enter selection [1-75] :
Compile for nesting? (0=no nesting, 1=basic,...) [default 0] :
```

For "real case" applications (not idealized ones), we recommend 67 (dm+sm) for HW nodes and 71 (dm+sm) for KNL nodes based on 
our experience; for example, 1280 MPI tasks with 4 OpenMP threads per rank (dm+sm) performs better than the pure MPI (dm) with 
2560 MPI ranks, both using the same number of nodes.

The following shows an example script to compile WRF for Cori KNL.

#### Example WRF build script for Cori

```bash
#!/bin/bash

imach="knl"

doclean_all=true #true if removing changes in the configure options or Registry files

doclean=false

runconf=true

docompile=false

debug=false


# WRF directories
mversion="v4.4"
#WRF-SIG project directory as example; accessible only by WRF-SIG members

wrfroot="/global/cfs/cdirs/m4232/model"
script_dir="/global/cfs/cdirs/m4232/scripts/build"

export WRF_DIR=${wrfroot}/${mversion}/WRF

#Modules --------------------------------------------------------------------
modversion="2022-12"  #denotes the time of major system update
loading_script="${script_dir}/load_modules_${modversion}_wrfsig.sh"
source ${loading_script} ${imach}

export NETCDF_classic=1               #use classic (CDF1) as default
export WRFIO_NCD_LARGE_FILE_SUPPORT=1 #use 64-bit offset format (CDF2) of netcdf files
export USE_NETCDF4_FEATURES=0         #do not use netcdf4 compression (serial), need hdf5 module
export PNETCDF_QUILT="0"  #0 for not using

#set environmental variables used by WRF build system, borrowing environmental variables 
#set by modules

export NETCDF=$NETCDF_DIR
export NETCDF_BIN="$NETCDF_DIR/bin"
export NETCDF_LIB="$NETCDF_DIR/lib"

export PNETCDF=$CRAY_PARALLEL_NETCDF_DIR

export LD_LIBRARY_PATH="/usr/lib64":${LD_LIBRARY_PATH}
export PATH=${NETCDF_BIN}:${PATH}
export LD_LIBRARY_PATH=${NETCDF_LIB}:${LD_LIBRARY_PATH}

##
###JASPER
#LD_LIBRARY_PATH has typo; lib instead of lib64
export JASPER_LIB="/global/common/software/nersc/cori-2022q1/spack/cray-cnl7-haswell/jasper-2.0.32-anryqh2/lib64"
export JASPER_INC="/global/common/software/nersc/cori-2022q1/spack/cray-cnl7-haswell/jasper-2.0.32-anryqh2/include"

echo "LD_LIBRARY_PATH: "$LD_LIBRARY_PATH
echo "PATH: "$PATH
echo "MANPATH: "$MANPATH

echo "NETCDF is $NETCDF"
echo "NETCDF_LIB is $NETCDF_LIB"

#echo "HDF5 is $HDF5"
#echo "HDF5_LIB is $HDF5_LIB"

echo "JASPER_LIB is $JASPER_LIB"
echo "JASPER_INC is $JASPER_INC"

echo "PNETCDF: ${PNETCDF}"
echo "PNETCDF_QUILT: ${PNETCDF_QUILT}"


##capture starting time for log file name
idate=$(date "+%Y-%m-%d-%H")
#

##run make in the top directory
cd $WRF_DIR

if [ "$doclean_all" = true ]; then
    ./clean -a
    #"The './clean –a' command is required if you have edited the configure.wrf 
    #or any of the Registry files.", but this deletes configure.wrf....
    
fi

if [ "$doclean" = true ]; then
    ./clean
fi

#echo "running configure"
if [ "$runconf" = true ]; then

    if [ "$debug" = true ]; then
        echo "configure debug mode"
        ./configure -d
    else
        ./configure
    fi

    configfile="${WRF_DIR}/configure.wrf"

	if [ -f "$configfile" ]; then
		echo "editing configure.wrf"
		#change "mpif90" "mpicc" to NERSC compiler wrapper
		sed -i "s/mpif90/ftn/" ${configfile}
		sed -i "s/mpicc/cc/" ${configfile}
		#keep serial-program compilers to be the base compilers ifort and icc; 
        #simplest way to compile for KNL on log-in node
        #sed -i "s/ifort/ftn/" ${configfile}
		#sed -i "s/icc/cc/" ${configfile}
		
        #other recommended (manual) changes for configure.wrf
		#1) "-xHost" flag (tell the compiler to use the KNL's wider instruction set than haswell
		#this edits allows us to compile WRF for KNL on the login node (which uses haswell cpu) with the
        #compiler wrappers ftn and cc
		# add to:  CC_TOOLS_CFLAGS -> does not appear in configure.default (L~230)
		# remove from:  CFLAGS_LOCAL, LDFLAGS_LOCAL
		#this is mainly for KNL
		
		#2) "-auto" flag 
		#Causes all local, non-SAVEd variables to be allocated to the run-time stack segment for temporal variables.
		#add this flag to FCBASEOPTS_NO_G 
		#and remove it from OMP and OMPCC 
		
		#3) remove the flag -DWRF_USE_CLM from ARCH_LOCAL if not plan to use CLM4 land model
		#to speed up compilation
		sed -i 's/-DWRF_USE_CLM/ /' ${configfile}  
		#https://unix.stackexchange.com/questions/32907/what-characters-do-i-need-to-escape-when-using-sed-in-a-sh-script

        #4)also add (uncomment) "-DRSL0_ONLY" to CFLAGS_LOCAL to supress 
        #rsl.err.xxxx and rsl.out.xxxx files from all MPI ranks except for 
        #rsl.err.0000 and rsl.out.000
        echo "using the DRSL0_ONLY flag; only the root rank will write the err and out files"
        sed -i 's/# -DRSL0_ONLY/-DRSL0_ONLY/' ${configfile}
		
	fi
    
fi


if [ "$docompile" = true ]; then
    export J="-j 6"  #build in parallel
    echo "J = $J"
    
    bldlog=${script_dir}/compile_em_${idate}_${imach}.log
    echo  "compile log file is ${bldlog}"
    
    #save modules into a log file
    module list &> ${bldlog}
    
    ./compile em_real &>> ${bldlog}
    #&> both the standard output (file descriptor 1) and the standard error output (2) to the log file
    
    grep "Problems building executables" compile_em_real_${idate}_${imach}.log
    RESULT=$?

    if [ $RESULT -eq 0 ]; then
        echo "compile failed, check ${bldlog}"      
    else
        echo "compile success"
        #sometimes renaming executable with more information is useful
        #cp $WRF_DIR/main/ideal.exe $WRF_DIR/main/ideal_${idate}_${imach}.exe
        #cp $WRF_DIR/main/real.exe $WRF_DIR/main/real_${idate}_${imach}.exe
        #cp $WRF_DIR/main/wrf.exe $WRF_DIR/main/wrf_${idate}_${imach}.exe
        #cp $WRF_DIR/main/ndown.exe $WRF_DIR/main/ndown_${idate}_${imach}.exe
    fi

fi
```

As seen in the example script, a couple of modifications are recommended for the configure.wrf file 
to compile for KNL nodes.

Specifically:

- remove  "-xHost" flag from:  CFLAGS_LOCAL and LDFLAGS_LOCAL
   tell the compiler to use the KNL's wider instruction set than Haswell)
- add "-xHost" flag to:  CC_TOOLS_CFLAGS 

These edits allow us to compile WRF for KNL on the login node (which uses Haswell cpu) with the compiler 
wrappers (ftn and cc). Another way is to use the base compiler for SFC and SCC (use compiler wrappers for 
DM_FC and DM_CC) in configure.wrf.

## Run WRF 

Experience of WRF-SIG members and [NERSC Best Practice](../../jobs/best-practices.md) recommends the following:

1. Use the scratch space for model execution and set an appropriate file strip on the execution directory.

1. Use the parallel netcdf library I/O
Time spent on history and restart file writing is reduced by at least 30%, often to be 1/10 of 
a serial netCDF I/O (need to set an appropriate [stripe size](../../performance/io/lustre/index.md)
 for the output directory).

1. Use four OpenMP threads instead of use all the available physical cores for MPI tasks. 
On Perlmutter for the COUNS 2.5km benchmark case, an 8-node job with 4 OpenMP threads and 
256 MPI ranks performs as almost equally fast as a 16-nodes job with 2048 MPI ranks without 
OpenMP threads. 

### Example WRF sbatch script for Perlmutter

```bash
#!/bin/bash 
#SBATCH -N 1
#SBATCH -q debug
#SBATCH -t 00:30:00
#SBATCH -J test
#SBATCH -A <account>
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=elvis@nersc.gov
##SBATCH -L SCRATCH,project
#SBATCH -C cpu
#SBATCH --tasks-per-node=64

pwd
#debug queue on perlmutter limited to max of 4 nodes
imach="pm"
icase="test" #name of simulation, used as the name of the execution directory
ntile=4  #number of OpenMP threads per MPI task
#need to set the "numtiles" variable in the wrf namelist (namelist.input) to be the same 

#example using the WRFSIG project CFS directories; 
#files only accesible by the WRFSIG members

bindir="/global/common/software/m4232/bin"
binname="wrf_2023-01-09-15_55_pm.exe"
rundir="/pscratch/sd/e/elvis/simulation/WRF/${icase}"

#Modules --------------------------------------------------------------------
modversion="2022-12"  #use year of major update that module (default) are introduced (INC0182147)
loading_script="/global/cfs/cdirs/m4232/scripts/build/load_modules_${modversion}_wrfsig.sh"
source ${loading_script} ${imach}

#OpenMP settings:
export OMP_NUM_THREADS=$ntile
export OMP_PLACES=threads
export OMP_PROC_BIND=spread
#export OMP_DISPLAY_AFFINITY=true
#export OMP_AFFINITY_FORMAT="host=%H, pid=%P, thread_num=%n, thread affinity=%A"

#MPI state and statistics
export MPICH_MPIIO_HINTS_DISPLAY=1
#export MPICH_ENV_DISPLAY=1
#export MPICH_MEMORY_REPORT=3 #can lead to a run-time error after the 2022-12 maintenance
export MPICH_MPIIO_AGGREGATOR_PLACEMENT_DISPLAY=1
export MPICH_MPIIO_STATS=1
export MPICH_STATS_DISPLAY=1

cd $rundir

mkdir -p logs

#capture starting time
tstart=$(date "+%s")

#run simulation
srun -n 64 -c 4 --cpu_bind=cores ${bindir}/${binname}

#capture error code
srunval=$?

#capture ending time
tend=$(date "+%s")

runtime=$((tend-tstart))

#rename and save the process 0 out and err files
cp rsl.error.0000 rsl.error_0_$SLURM_JOB_ID
cp rsl.out.0000 rsl.out_0_$SLURM_JOB_ID

```

To set appropriate options for the srun command (by considering 
[process and thread affinity ](../../jobs/affinity/index.md)), 
users are encouraged to use the jobscript generator available in the MyNERSC website. 

## Reference

[Balle, T., & Johnsen, P. (2016). Improving I / O Performance of the Weather Research and Forecast ( WRF ) Model. Cray User Group.](https://cug.org/proceedings/cug2016_proceedings/includes/files/pap123s2-file1.pdf)
